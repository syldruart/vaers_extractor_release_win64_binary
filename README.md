# How to use the vaers vaccine adverse event analyser application: #

## Download the application ##
On this bitbucket repository page of the project, go to "Downloads" on the left pane and click "download repository".
Save the zip in a well chosen location and extract the application exe file with the readme, don't care about the .gitignore.
Try to launch the exe. If any antivirus blocks it, try to allow the application in the antivirus. This not a virus, neither the covid.

## Use instructions ##

If you already have generated the vaers data, open application and start step 4)

### 1) First download the official USA vaers and CDC database files: ###

VAERS reports: https://vaers.hhs.gov/data/datasets.html?
Download zip files of 2020, 2021 and 2022 and unzip them at the same location than the application executable file.
You should have 9 files:

* 2020VAERSDATA.csv
* 2021VAERSDATA.csv
* 2022VAERSDATA.csv
* 2020VAERSSYMPTOMS.csv
* 2021VAERSSYMPTOMS.csv
* 2022VAERSSYMPTOMS.csv
* 2020VAERSVAX.csv
* 2021VAERSVAX.csv
* 2022VAERSVAX.csv

VACCINE distribution and administration by US jusridiction: https://data.cdc.gov/Vaccinations/COVID-19-Vaccinations-in-the-United-States-Jurisdi/unsk-b7fc
Click on Export, select csv and put the file at the same location than the application executable file.
You should have one file:

* COVID-19_Vaccinations_in_the_United_States_Jurisdiction.csv

### 2) Open the exe application and generate vaers formatted file ###

As the formatted data is not yet generated, you will see a red sticker as this file does not exist yet. 
Click on "generate formatted file" and select the folder of the application (the downloaded are supposed to be there).
The process may take several minutes; a task progress is shown, be patient.
At the end of the process, a new file called VAERS_FORMATTED_DATA.csv should have been created. The sticker should switch to green.

### 3) Check vaccine distribution file. As this should have been downloaded and put in the application folder, the sticker should be green. ###

Note:
For steps 2) and 3) you may also browse file locations. Anyway the file names must be respected.

### 4) Initialize data ###

Click on "Initialize data" and wait process to be finished (should be fast)

### 5) Compute results: ###

Fields explanation:

* Symptom name: Type a symptom or a symptom equation, let blank if you want to ignore this field, the program will thus count every symptom.
* Vaccine type: count the selected vaccine only
* Onset delay max: count the case where onset delay is lower than the quantity specified in the field (def. 500)
* Vaccine lots: enter the vaccine lot name you want to detect reported adverse envents, let blank if you want to ignore this field, the program will thus count every lot.
* Vaccine dose: count the selected dose number only
* Age range: count cases where patient age is included in your specified limits
* Male/Female: filter patient's genre
* Death case, Doctor visits, Hospitalizations, Emergency cares, Life threatening check box: the program will count the checked options. If you let all options not checked, the program will count all cases.

examples: 

a) "Myocarditis", the program will count each case having encountered a myocarditis

b) "Myocarditis OR Pericarditis", the program will count each case having encountered a myocarditis or a pericarditis, it will avoid counting twice the case if a suspect has both symptoms.

c) "Myocarditis OR Pericarditis AND Chills", the program will count each case having encountered a myocarditis or a pericarditis and will take only those which have encountered chills.

Note: the OR has highest priority than AND. You can not make brackets expressions.

Click on Run results to display the following: Make window screenshots to save the results
All popup of histograms will be shown, 8 in total:

* All vaccine age distribution histogram
* All vaccine onset delay distribution histogram
* Pfizer age distribution histogram
* Pfizer onset delay distribution histogram
* Moderna age distribution histogram
* Moderna onset delay distribution histogram
* Janssen age distribution histogram
* Janssen onset delay distribution histogram
   
In the global result group frame:

* Number of cases: display the detected number of cases following your field selection
* Median age: compute the median age related to the results of your field selection
* Median delay: compute the median onset delay related to the results of your field selection

A folder <Results_x> containing the following files will be created. The folder index is incremented to avoid being overwritten.

* Individual report file: location of a file where all the individual reports of the results will be stored.
* Symptom result file: location of a file where all the symptoms cumulated by decreasing order of the results will be stored
  Note: this helps to find a specific symptom name and recompute the results with the symptom name you want.
* Vaccine lots result file: location of a file where all the vax lots cumulated by decreasing order of the results will be stored.
  Note: this helps to find a specific vaccine lots name and recompute the results with the symptom name you want.
  
### 6) Run incidences: Make window screenshots to save the results ###

When you click on "Run incidences", the program will take time to calculate cases incidences related to your the fields you selected. 
After a while, 4 tables will be displayed showing the number of cases, number of people that are totally vaccinated and the related incidences.

* A table for all vaccines
* One for pfizer
* One for Moderna
* One for Janssen